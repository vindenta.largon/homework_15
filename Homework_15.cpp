﻿

#include <iostream>
#include <cmath>

class Stack
{
private:
    int size = 2;
    int stack = 0;
    int currentSize = 0;
    int* arr = nullptr;
    int* newArrey = nullptr;

public:
    Stack()
    {
        arr = new int[size];
        currentSize = size;
    }

    Stack(int firstSteck)
    {
        currentSize = size;
        arr = new int[currentSize];
        arr[stack] = firstSteck;
        stack++;
        
    }

    int push(int newSteck)
    {
        if (stack >= 0 && stack < currentSize)
        {
            arr[stack] = newSteck;
            stack++;
        }

        else if (stack >= currentSize)
        {
            currentSize = currentSize + size;
            newArrey = new int[currentSize];
            for (int i = 0; i < stack; i++)
            {
                newArrey[i] = arr[i];
            }
            delete[] arr;
            arr = nullptr;
            arr = new int[currentSize];
            for (int i = 0; i < stack; i++)
            {
                arr[i] = newArrey[i];
            }
            delete[] newArrey;
            newArrey = nullptr;
            
            arr[stack] = newSteck;
            stack++;
        }
        return arr[stack-1];
    }

    int pop()
    {

        if (stack <= currentSize - size)
        {
            currentSize = currentSize - size;
            newArrey = new int[currentSize];
            for (int i = 0; i < stack; i++)
            {
                newArrey[i] = arr[i];
            }
            delete[] arr;
            arr = nullptr;
            arr = new int[currentSize];
            for (int i = 0; i < stack; i++)
            {
                arr[i] = newArrey[i];
            }
            delete[] newArrey;
            newArrey = nullptr;
        }
        stack--;
        return arr[stack];
    }

    void GetSteck()
    {
        std::cout <<"last stack number: "<< stack << " ; " << "steck filling: "<< arr[stack-1]<< "\n";
    }

    void GetAllSteck()
    {
        for (int i=0, n=1; i<stack; i++, n++)
        {
            std::cout << arr[i] << "\n";
        }
    }

    void GetCurrentSize()
    {
        std::cout <<"current size: " << currentSize << "\n";
    }

    int GetLastStackNumber()
    {
        return stack;
    }

};




int main()
{
    Stack s;
    s.GetCurrentSize(); //проверка размера начального массива
    std::cout << "add stack number: " << s.GetLastStackNumber() << " ; " << "add stack filling: " << s.push(12) << "\n"; //проверка данных первого push
    s.push(52);
    s.push(61);
    s.push(33);
    s.push(94);
    s.GetSteck(); //проверка сохраниения данных о последнем стеке
    s.GetCurrentSize(); //проверка увеличения размера массива
    std::cout << "extractable stack number: " << s.GetLastStackNumber() << " ; " << "extractable stack filling: " << s.pop() << "\n"; //проверка данных первого pop
    s.GetSteck(); //проверка изменения информации о последнем стеке
    s.pop();
    s.pop();
    s.GetCurrentSize(); //проверка уменьшения размера массива
    s.GetSteck(); //проверка информации о последнего стеке
    s.GetAllSteck(); //проверка заполнения стека после всех операций
    

}